#!/bin/bash

export LANG_DEPEND="nodejs"

#*******************************************************************************

export COFFEEPATH="$LANG_PATH/dist"

################################################################################

ronin_require_coffee () {
    echo hello world >/dev/null
}

ronin_include_coffee () {
    echo "    -> Coffee   : "$COFFEEPATH
}

################################################################################

ronin_setup_coffee () {
    echo hello world >/dev/null
}

